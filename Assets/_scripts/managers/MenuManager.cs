using System.Threading.Tasks;
using UnityEngine;

public class MenuManager : MonoBehaviour
{
    [SerializeField] private RectTransform TitleText;
    [SerializeField] private RectTransform StartButton;
    [SerializeField] private GameObject FightText;

    private void Awake()
    {
        GameManager.GameStateChanged += OnGameStateChanged;
        FightText.SetActive(false);
    }

    private void OnDestroy()
    {
        GameManager.GameStateChanged -= OnGameStateChanged;
    }

    private void OnGameStateChanged(GameState objGameState)
    {
        if (objGameState == GameState.BeforeMenu)
        {
            Debug.Log("state changed to" + objGameState);
            LeanTween.moveY(TitleText, -100.0f, 1.0f).setEaseOutQuad();
            LeanTween.moveY(StartButton, 80.0f, 1.0f).setEaseOutQuad();
            GameManager.Instance.UpdateGameState(GameState.Menu);
            return;
        }
    }

    public async void OnStartButtonClick()
    {
        LeanTween.moveY(TitleText, 100.0f, 1.0f).setEaseInQuad();
        LeanTween.moveY(StartButton, -80.0f, 1.0f).setEaseInQuad();
        await Task.Delay(1000);
        FightText.SetActive(true);
        await Task.Delay(1000);
        FightText.SetActive(false);
        GameManager.Instance.UpdateGameState(GameState.InGame);
    }
}
