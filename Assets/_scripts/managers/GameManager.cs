using System;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }
    private GameState gameState;
    public static event Action<GameState> GameStateChanged; 

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject); //dont Allow 2
        }
    }
    
    private void Start()
    {
        UpdateGameState(GameState.BeforeMenu);
    }

    public void UpdateGameState(GameState NewState)
    {
        gameState = NewState;
        switch (gameState)
        {
            case GameState.BeforeMenu:
                break;
            case GameState.Menu:
                break;
            case GameState.InGame:
                break;
            case GameState.EndGame:
                break;
            case GameState.Win:
                break;
            case GameState.Lose:
                break;
        }
        GameStateChanged?.Invoke(gameState);
    }
}

