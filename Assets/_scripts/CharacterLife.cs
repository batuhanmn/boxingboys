using UnityEngine;
using UnityEngine.UI;

public class CharacterLife : MonoBehaviour
{
    [SerializeField] private Slider LifeSlider;
    [SerializeField] private Transform instantiatePoint;
    [SerializeField] private GameObject bloodPrefab;

    private float life = 100;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void updateLife(float damage)
    {
        life = life - damage;
        if (life < 0)
        {
            life = 10;
        }

        LifeSlider.value = life / 100.0f;
        Destroy(Instantiate(bloodPrefab, instantiatePoint.position, instantiatePoint.rotation),5);
        
    }
}
