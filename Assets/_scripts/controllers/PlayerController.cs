using UnityEngine;
using UnityEngine.Assertions.Must;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private CharacterAnimationController _characterAnimationController;
    [SerializeField] private SphereCollider rightGloveCollider;
    [SerializeField] private SphereCollider leftGloveCollider;

    void Update()
    {
        if (Input.GetKey(KeyCode.Q))
        {
            leftGloveCollider.enabled = true;
            _characterAnimationController.playAnimation(AnimationName.LeftHook);
            Debug.Log("Left hook");
            return;
        }
        if (Input.GetKey(KeyCode.E))
        {
            rightGloveCollider.enabled = true;
            _characterAnimationController.playAnimation(AnimationName.RightHook);
            Debug.Log("Right Hook");
            return;
        }
        if (Input.GetKey(KeyCode.A))
        {
            leftGloveCollider.enabled = true;
            _characterAnimationController.playAnimation(AnimationName.LeftDirect);
            Debug.Log("Left Direct");
            return;
        }
        if (Input.GetKey(KeyCode.D))
        {
            rightGloveCollider.enabled = true;
            _characterAnimationController.playAnimation(AnimationName.RightDirect);
            Debug.Log("Right Direct");
            return;
        }
        if (Input.GetKey(KeyCode.Z))
        {
            _characterAnimationController.playAnimation(AnimationName.LeftDodge);
            Debug.Log("Left Dodge");
            return;
        }
        if (Input.GetKey(KeyCode.C))
        {
            _characterAnimationController.playAnimation(AnimationName.RightDodge);
            Debug.Log("Right dodge");
            return;
        }
        if (Input.GetKey(KeyCode.W))
        {
            _characterAnimationController.playAnimation(AnimationName.Taunt);
            Debug.Log("Taunt");
            return;
        }
    }
}
