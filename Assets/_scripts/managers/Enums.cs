public enum GameState
{
    BeforeMenu,
    Menu,
    InGame,
    EndGame,
    Win,
    Lose
}

public enum AnimationName
{
    Idle,
    LeftHook,
    RightHook,
    LeftDirect,
    RightDirect,
    LeftDodge,
    RightDodge,
    Taunt,
    Win,
    Lose,
    LeftHeadHit,
    RightHeadHit,
    LeftBodyHit,
    RightBodyHit,
    Drop,
    GetUp
}
