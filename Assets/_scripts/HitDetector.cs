
using UnityEditor.Recorder;
using UnityEngine;

public class HitDetector : MonoBehaviour
{
    [SerializeField] private CharacterAnimationController myAnimController;
    [SerializeField] private CharacterLife myLifeController;

    private float delay = 0.5f;

    private bool delayHit = false;
    // Start is called before the first frame update
    void Start()
    {
        resetDelay();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void LateUpdate()
    {
        if (!delayHit)
        {
            return;
        }

        delay = delay - Time.deltaTime;
        if (delay < 0)
        {
            resetDelay();
        }
    }

    private void resetDelay()
    {
        delay = 1.0f;
        delayHit = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (delayHit)
        {
            other.enabled = false;
            return;
        }
        Debug.Log(other.tag);
        if (other.CompareTag("LeftGlove"))
        {
            if (CompareTag("Body"))
            {
                myAnimController.playAnimation(AnimationName.LeftBodyHit,true);
                delayHit = true;
                Debug.Log("left body hit");
                myLifeController.updateLife(10.0f);
                other.enabled = false;
                return;
            }

            if (CompareTag("Head"))
            {
                myAnimController.playAnimation(AnimationName.LeftHeadHit,true);
                delayHit = true;
                myLifeController.updateLife(5.0f);
                other.enabled = false;
                Debug.Log("left head hit");
                return;
            }
            return;
        }

        if (other.CompareTag("RightGlove"))
        {
            if (CompareTag("Body"))
            {
                myAnimController.playAnimation(AnimationName.RightBodyHit,true);
                delayHit = true;
                myLifeController.updateLife(10.0f);
                other.enabled = false;
                Debug.Log("Right body hit");
                return;
            }

            if (CompareTag("Head"))
            {
                myAnimController.playAnimation(AnimationName.RightHeadHit,true);
                delayHit = true; 
                myLifeController.updateLife(5.0f);
                other.enabled = false;
                Debug.Log("Right head hit");
                return;
            }
            return;
        }
        
    }
}
