using Animancer;
using UnityEngine;

public class CharacterAnimationController : MonoBehaviour
{
    [SerializeField] private AnimancerComponent _Animancer;

    [SerializeField] private AnimationClip _Idle;
    [SerializeField] private AnimationClip _LeftHook;
    [SerializeField] private AnimationClip _RightHook;
    [SerializeField] private AnimationClip _LeftDirect;
    [SerializeField] private AnimationClip _RightDirect;
    [SerializeField] private AnimationClip _LeftDodge;
    [SerializeField] private AnimationClip _RightDodge;
    [SerializeField] private AnimationClip _Taunt;
    [SerializeField] private AnimationClip _LeftHeadHit;
    [SerializeField] private AnimationClip _RightHeadHit;
    [SerializeField] private AnimationClip _LeftBodyHit;
    [SerializeField] private AnimationClip _RightBodyHit;
    [SerializeField] private AnimationClip _Drop;
    [SerializeField] private AnimationClip _Win;
    [SerializeField] private AnimationClip _Lose;
    [SerializeField] private AnimationClip _GetUp;
    
    [SerializeField] private SphereCollider rightGloveCollider;
    [SerializeField] private SphereCollider leftGloveCollider;

    private bool animationPlaying=false;
    private AnimancerState state;
    
    private void OnEnable()
    {
        // On startup, play the idle animation.
        _Animancer.Play(_Idle);
    }

    public void playAnimation(AnimationName animName, bool breakAnimation=false)
    {
        if (animationPlaying && !breakAnimation)
        {
            return;
        } 
        
        switch (animName)
        {
            case AnimationName.LeftHook:
                state = _Animancer.Play(_LeftHook);
                break;
            case AnimationName.RightHook:
                state = _Animancer.Play(_RightHook,0.25f);
                break;
            case AnimationName.LeftDirect:
                state = _Animancer.Play(_LeftDirect);
                break;
            case AnimationName.RightDirect:
                state = _Animancer.Play(_RightDirect,0.25f);
                break;
            case AnimationName.LeftDodge:
                state = _Animancer.Play(_LeftDodge);
                break;
            case AnimationName.RightDodge:
                state = _Animancer.Play(_RightDodge,0.25f);
                break;
            case AnimationName.Taunt:
                state = _Animancer.Play(_Taunt);
                break;
            case AnimationName.GetUp:
                state = _Animancer.Play(_GetUp);
                break;
            case AnimationName.Win:
                state = _Animancer.Play(_Win);
                break;
            case AnimationName.Lose:
                state = _Animancer.Play(_Lose);
                break;
            case AnimationName.LeftHeadHit:
                state = _Animancer.Play(_LeftHeadHit,0.25f);
                break;
            case AnimationName.RightHeadHit:
                state = _Animancer.Play(_RightHeadHit,0.25f);
                break;
            case AnimationName.LeftBodyHit:
                state = _Animancer.Play(_LeftBodyHit,0.25f);
                break;
            case AnimationName.RightBodyHit:
                state = _Animancer.Play(_RightBodyHit,0.25f);
                break;
            case AnimationName.Drop:
                state = _Animancer.Play(_Drop);
                break;
            default:
                state = _Animancer.Play(_Idle);
                break;
        }

        animationPlaying = true;

        // The Play method returns the AnimancerState which manages that animation so you can access and
        // control various details, for example:
        // state.Time = 1;// Skip 1 second into the animation.
        // state.NormalizedTime = 0.5f;// Skip halfway into the animation.
        // state.Speed = 2;// Play the animation twice as fast.
        
        // In this case, we just want it to call the OnActionEnd method (see below) when the animation ends.
        state.Events.OnEnd = OnActionEnd;
    }
    private void OnActionEnd()
    {
        animationPlaying = false;
         _Animancer.Play(_Idle, 0.25f);
    }
    
}
